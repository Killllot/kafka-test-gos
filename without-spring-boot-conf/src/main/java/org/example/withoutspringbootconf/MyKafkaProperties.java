package org.example.withoutspringbootconf;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "my.kafka")
@PropertySource("classpath:application.properties")
@Component
@Data
public class MyKafkaProperties {
    private String newPoolIntervalMs;
}
